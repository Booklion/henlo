#pragma once
#include "StudentRequest.h"
#include<ostream>

class OnlineSession :public StudentRequest {
public:
	// Copy constructor, assignment operator? because of m_url!!!
	
	// Default constructor
	OnlineSession() = default;

	//Copy constructor
	OnlineSession(const OnlineSession& other);

	// Assignment operator
	OnlineSession& operator=(const OnlineSession& other);

	// Overriding the display function from base class StudentRequest
	virtual ostream& display(ostream& os) override;

	// Destructor
	~OnlineSession() {}

	//Getter for the URL
	char* getURLOnline();

	// Setter for URL
	void setURLOnline(char* newURL);

	// Getter for the duration
	int getDurationInMin();

	// Setter for the duration
	void setDurationInMin(int newDuration);

private:
	char* m_url;
	int m_durationMin;
};

