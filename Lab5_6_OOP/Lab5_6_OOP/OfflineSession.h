#pragma once
#include "StudentRequest.h"

class OfflineSession :public StudentRequest {
public:
	//copy constructor, assignment operator, because of m_deliverableURL
	
	// Default constructor
	OfflineSession();

	// Copy constructor
	OfflineSession(const OfflineSession& other);

	// Destructor
	~OfflineSession();

	// Assignment operator
	OfflineSession& operator=(const OfflineSession& other);

	//Overriding the display function from base class StudentRequest
	virtual ostream& display(ostream& os) override;

	// Getter for the deliverable URL
	char* getDeliverableURL();

	// Setter for deliverable URL
	void setDeliverableURL();

	// Getter for the duration in minutes
	int getDuration();

	// Setter for the duration in minutes
	void setDuration(int newDuration);

private:
	char* m_deliverableURL;
	int m_durationMin;
};

