#include "StudentRequest.h"
#include "OfflineSession.h"
#include "OnlineSession.h"
#include<iostream>
using namespace std;


int main() {
	StudentRequest* requestArray = new StudentRequest[100];
	StudentRequest** polymorphicArray = new StudentRequest* [100];
	
	OfflineSession s1, s2, s3;
	OnlineSession o1, o2;  //do the constructor

	polymorphicArray[0] = &s1;
	polymorphicArray[1] = &s2;
	polymorphicArray[2] = &s3;

	polymorphicArray[3] = &o1;
	polymorphicArray[4] = &o2;

	for (int i = 0; i < 5; i++) {
		cout << *polymorphicArray[i]<<" " ;
	}
	return 0;
}
