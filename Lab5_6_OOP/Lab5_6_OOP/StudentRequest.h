#pragma once
#include "Date.h"
#include<ostream>
using namespace std;

class StudentRequest {
public: 
	//!!! NICIODATA NU FACEM COONSTRUCTORUL VIRUAL!!!
	
	// Default constructor
	StudentRequest();

	// Copy constructor
	StudentRequest(const StudentRequest& other);

	// Assignment operator
	StudentRequest& operator=(const StudentRequest& other);

	// Destructor - always make it virtual
	virtual ~StudentRequest();

	// Overloading the << operator
	virtual ostream& display(ostream& os);
	friend ostream& operator<<(ostream& os, StudentRequest&);

	// Getter for ID
	int getID();

	// Setter for ID
	void setID(int newID);

	// Getter for date
	Date getDate();

	// Setter for date
	void setDate(Date newDate);

	// Getter for subject
	char getSubject();

	// Setter for subject
	void setSubject(char* newSubject);

private:
	int m_id;
	Date m_date;
	char* m_subject;
	typedef enum {
		NOT_PAID,
		PENDING_COMPLETION,
		COMPLETED
	}Status; //0 - not paid, 1 - paid, pending completion, 2 - completed
};

