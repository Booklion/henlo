#include "StudentRequest.h"


StudentRequest::StudentRequest() {
	m_date = { 1,1,1999 };
	m_id = 1000;
	m_subject = new char;
}

StudentRequest::StudentRequest(const StudentRequest& other) {
	m_date = other.m_date;
	m_id = other.m_id;
	m_subject = other.m_subject;
}

StudentRequest& StudentRequest::operator=(const StudentRequest& other) {
	m_date = other.m_date;
	m_id = other.m_id;
	m_subject = other.m_subject;
	return *this;
}

StudentRequest::~StudentRequest() {
	delete &this->m_date;
	//this->m_date = nullptr;
	delete& this->m_id;
	delete& this->m_subject;
}

ostream& StudentRequest::display(ostream& os) {
	os << "Student request; subject: " << m_subject << "for student with ID: " << m_id;
	return os;
}

int StudentRequest::getID() {
	return m_id;
}

void StudentRequest::setID(int newID) {
	m_id = newID;
}

Date StudentRequest::getDate() {
	return m_date;
}

void StudentRequest::setDate(Date newDate) {
	m_date = newDate;
}

char StudentRequest::getSubject() {
	return *m_subject;
}

void StudentRequest::setSubject(char* newSubject) {
	m_subject = newSubject;
}

ostream& operator<<(ostream& os, StudentRequest& s) {
	return s.display(os);
}


