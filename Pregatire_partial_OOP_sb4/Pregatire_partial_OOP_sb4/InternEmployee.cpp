#include "InternEmployee.h"

InternEmployee::InternEmployee(char* id, char* name, int birthYear, Position pos,int internshipDuration) : Employee(id, name, birthYear, pos) {
	m_internshipDuration = internshipDuration;
}

int InternEmployee::getInternshipDuration() const
{
	return m_internshipDuration;
}

void InternEmployee::setInternshipDuration(int internshipDuration)
{
	this->m_internshipDuration = internshipDuration;
}

void InternEmployee::displayInfo()
{
	Employee::displayInfo();
	cout << "This is our intern employee: " << Employee::getName() << ". His/Her internship duration is: " << m_internshipDuration;
	if (m_position == MANAGER) {
		cout << " and he/she is a manager here." << endl;
	}
	if (m_position == DEVELOPER) {
		cout << " and he/she is a developer here." << endl;
	}
	if (m_position == TESTER) {
		cout << " and he/she is a tester here." << endl;
	}
}
