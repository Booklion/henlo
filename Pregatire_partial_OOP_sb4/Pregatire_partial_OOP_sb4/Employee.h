#pragma once
#include<string>
#include<iostream>
using namespace std;

typedef enum  {
	MANAGER,
	DEVELOPER,
	TESTER
}Position;

class Employee {
public:
	Employee(char* id, char* name, int birthYear, Position pos);

	Employee(const Employee& other);

	Employee& operator=(const Employee& other);

	virtual ~Employee();

	char* getID() const;

	char getName() const;

	void setName(char* newName);

	int getBirthYear() const;

	void setBirthYear(int newBirthYear);

	Position getPosition() const;

	void setPosition(Position newPosition);

	virtual void displayInfo();

	//char toString(char* string);

protected:
	Position m_position;

private: 
	char* m_id; //unique
	char* m_name;
	int m_birthYear;
};

