#include "Employee.h"

Employee::Employee(char* id, char* name, int birthYear, Position pos) {
	this->m_name = new char[strlen(name) + 1];
	for (unsigned int i = 0; i < strlen(name); i++) {
		this->m_name[i] = name[i];
	}
	this->m_name[strlen(name)] = '\0';

	this->m_birthYear = birthYear;

	this->m_id = new char[strlen(id)+1];
	for (unsigned int i = 0; i < strlen(id); i++) {
		this->m_id[i] = id[i];
	}
	this->m_id[strlen(id)] = '\0';

	m_position = TESTER;
}

Employee::Employee(const Employee& other)
{
	this->m_birthYear = other.m_birthYear;

	this->m_name = new char[strlen(other.m_name) + 1];
	for (unsigned int i = 0; i < strlen(other.m_name); i++) {
		this->m_name[i] = other.m_name[i];
	}
	this->m_name[strlen(other.m_name)] = '\0';

	this->m_id = new char[strlen(other.m_id) + 1];
	for (unsigned int i = 0; i < strlen(other.m_id); i++) {
		this->m_id[i] = other.m_id[i];
	}
	this->m_id[strlen(other.m_id)] = '\0';
	this->m_position = other.m_position;
}

Employee& Employee::operator=(const Employee& other)
{
	if (this != &other) {
		delete[] this->m_id;
		this->m_id = nullptr;

		this->m_birthYear = other.m_birthYear;

		this->m_name = new char[strlen(other.m_name) + 1];
		for (unsigned int i = 0; i < strlen(other.m_name); i++) {
			this->m_name[i] = other.m_name[i];
		}
		this->m_name[strlen(other.m_name)] = '\0';

		this->m_id = new char[strlen(other.m_id) + 1];
		for (unsigned int i = 0; i < strlen(other.m_id); i++) {
			this->m_id[i] = other.m_id[i];
		}
		this->m_id[strlen(other.m_id)] = '\0';
		this->m_position = other.m_position;
	}
	return *this;
}

Employee::~Employee()
{
	delete[] this->m_id;
	this->m_id = nullptr;

	delete[] this->m_name;
	this->m_name = nullptr;
}

char* Employee::getID() const
{
	return this->m_id;
}

char Employee::getName() const
{
	return *this->m_name;
}

void Employee::setName(char* newName)
{
	this->m_name = newName;
}

int Employee::getBirthYear() const
{
	return m_birthYear;
}

void Employee::setBirthYear(int newBirthYear)
{
	m_birthYear = newBirthYear;
}

Position Employee::getPosition() const
{
	return m_position;
}

void Employee::setPosition(Position newPosition)
{
	this->m_position = newPosition;
}

void Employee::displayInfo()
{
	cout << "This is employee " << m_name << " with ID " << m_id << ", born in " << m_birthYear<<". He/She is a";
	if (m_position == MANAGER) {
		cout << " manager here." << endl;
	}
	if (m_position == DEVELOPER) {
		cout << " developer here." << endl;
	}
	if (m_position == TESTER) {
		cout << " tester here." << endl;
	}
}
/*
char Employee::toString(char* string)
{
	Employee* result;
	if (strpbrk(this->m_name, string) != NULL) {
		for (int i = 0; i < 100; i++) {
			result[i] = *this;
		}
	}
	else {

	}
}
*/
