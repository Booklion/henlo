#include "PartTimeEmployee.h"

PartTimeEmployee::PartTimeEmployee(char* id, char* name, int birthYear, Position pos, int workingHour) : Employee(id, name, birthYear, pos) {
	m_workingHour = workingHour;
}

int PartTimeEmployee::getWorkingHour() const
{
	return m_workingHour;
}

void PartTimeEmployee::setWorkingHour(int newWorkingHour)
{
	this->m_workingHour = newWorkingHour;
}

void PartTimeEmployee::displayInfo()
{
	Employee::displayInfo();
	cout << "This is part-time employee: " << Employee::getName() << ". His/Her working hour is: " << m_workingHour;
	if (m_position == MANAGER) {
		cout << " and he/she is a manager here." << endl;
	}
	if (m_position == DEVELOPER) {
		cout << " and he/she is a developer here." << endl;
	}
	if (m_position == TESTER) {
		cout << " and he/she is a tester here." << endl;
	}
}
