#pragma once
#include "Employee.h"
class PartTimeEmployee :public Employee {
public:
	PartTimeEmployee(char* id, char* name, int birthYear, Position pos, int workingHour);

	int getWorkingHour() const;

	void setWorkingHour(int newWorkingHour);

	void displayInfo();

private: 
	int m_workingHour;
};

