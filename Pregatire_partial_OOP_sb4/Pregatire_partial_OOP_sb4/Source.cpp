#include "Employee.h"
#include "InternEmployee.h"
#include "PartTimeEmployee.h"
#include<iostream>
#include<string>
using namespace std;

int main() {
	char id1[] = "1";
	char name1[] = "Jonathan";
	InternEmployee e1{ id1, name1, 1982, DEVELOPER, 3 };
	e1.setPosition(DEVELOPER);

	char id2[] = "2";
	char name2[] = "Erina";
	Employee e2{ id2, name2, 1990, TESTER };

	char id3[] = "3";
	char name3[] = "Speedwagon";
	PartTimeEmployee e3{ id3, name3, 1995, MANAGER, 4 };
	e3.setPosition(MANAGER);

	e1.displayInfo();
	e2.displayInfo();
	e3.displayInfo();




	return 0;
}
