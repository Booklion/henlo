#pragma once
#include "Employee.h"
class InternEmployee : public Employee {
public: 
	InternEmployee(char* id, char* name, int birthYear, Position pos , int internshipDuration);

	int getInternshipDuration() const;

	void setInternshipDuration(int internshipDuration);

	void displayInfo();

private: 
	int m_internshipDuration;
};

