#pragma once
#include<ostream>
class DynamicArray{
public: 
	DynamicArray(unsigned int initialCap); //constructor

	//copy constructor
	DynamicArray(const DynamicArray& other);

	void insert(void* elem);

	void deleteLast();

	void display(void (*displayElement)(void*));

	void release();  //the destructor

	int search(bool (*condition)(void*));

	//friend ostream& operator<<(ostream& os, const DynamicArray& arr);

private: 
	int len;
	int cap;
	void** data;
};

