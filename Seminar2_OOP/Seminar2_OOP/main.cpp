#include "dynamic_array.h"
#include<stdlib.h>
#include<iostream>
using namespace std;


typedef struct {
	float r, i;
} complex;

bool isEven(void* elem) {
//	(int*)eleme - > int*
	return *((int*)elem )% 2 == 0;
}

bool isNegative(void* elem) {
	return *((int*)elem) < 0;
}

void printInt(void* a) {
	int* ai = (int*) a;
	cout << *ai<<" ";
}

void printComplex(void* a) {
	complex* ai = (complex*)a;
	cout << ai->r<<" "<<ai->i << "i; ";
}



int main() {
	DynamicArray* test = create(10);

	DynamicArray* arrCOmplex = create(10);
	complex* el = new complex;
	el->i = 10;
	el->r = 20;
	insert(arrCOmplex, (void*)el);
	display(arrCOmplex, printComplex);

	display(test, printInt);
	for (int i = 0; i < 30; i++) {
		int* a = new int;
		*a = rand() % 20;
		insert(test,(void*)a);
		if (i % 10 == 0) {
			display(test, printInt);
		}
	}
	cout <<"The index of the first number: "<< search(test, isEven) << endl;
	cout << search(test, isNegative);

	release(test);
	return 0;
}
