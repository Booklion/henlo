#include "DynamicArray.h"
#include<iostream>
using namespace std;

DynamicArray::DynamicArray(unsigned int initialCap) {
	len = 0;
	cap = initialCap;
	data = new void* [initialCap];
}

DynamicArray::DynamicArray(const DynamicArray& other){
	data = new void* [other.cap];
	this->len = other.len;
	this->cap = other.cap;
	for (int i = 0; i < this->len; i++) {
		data[i] = other.data[i];
	}
}

void DynamicArray::insert(void* elem) {
	if (this->len == this->cap) {
		this->cap = this->cap * 2;
		void** newArray = new void* [this->cap];
		for (unsigned int i = 0; i < this->len; i++) {
			newArray[i] = this->data[i];
		}
		delete[] this->data;
		this->data = nullptr;
		this->data = newArray;
	}
	this->data[this->len++] = elem;
}

void DynamicArray::deleteLast() {
	delete this->data[len - 1];
}

void DynamicArray::display(void (*displayElement)(void*)) {
	for (unsigned int i = 0; i < this->len; i++) {
		displayElement(this->data[i]);
	}
	cout << endl;
}

void DynamicArray::release() {
	for (int i = 0; i < this->len; i++)
		delete this->data[i];
	delete[] this->data;
	this->data = nullptr;
}

int DynamicArray::search(bool (*condition)(void*)) {
	for (unsigned int i = 0; i < this->len; i++) {
		if (condition(this->data[i])) {
			return i;
		}
	}
	return -1;
}

//ostream& operator<<(ostream& os, const DynamicArray& arr){
	
//}
 

//create a dynamic array of complex numbers