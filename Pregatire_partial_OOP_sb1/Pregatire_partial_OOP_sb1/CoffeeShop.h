#pragma once
#include "Person.h"
#include "Student.h"
#include<ostream>
#include<iostream>
using namespace std;

class CoffeeShop
{
public: 
	CoffeeShop(int capacity);

	void enqueue(Person* p);

	void serveClients();

	friend ostream& operator<<(ostream& os, const CoffeeShop& other);

private:
	int m_cap;
	int m_nrPeople;
	Person** m_people;
};

