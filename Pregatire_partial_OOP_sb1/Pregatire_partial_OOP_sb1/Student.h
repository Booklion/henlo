#pragma once
#include "Person.h"

class Student : public Person
{
public:
	Student();

	Student(char* id, char* firstName, char* lastName, int enrollmentNumber);

	void displayInfo();

	friend ostream& operator<<(ostream& os, const Student& other);

	int getEnrollmentNumber()const;


private:
	int m_enrollmentNumber;
};

