#pragma once
#include<string>
#include<ostream>
#include<iostream>
using namespace std;

typedef enum {
	CIVILIAN,
	STUDENT
}PersonType;

class Person {
public:
	Person();

	Person(char* id, char* firstName, char* lastName);

	Person(const Person& other);

	virtual ~Person();

	Person& operator=(const Person& other);

	char* getID() const;

	char* getFirstName() const;
	void setFirstName(char* newFirstName);

	char* getLastName() const;
	void setLastName(char* newLastName);

	PersonType getType() const;

	friend ostream& operator<<(ostream& os, const Person& p);

	static int getNrObjects();

	virtual void displayInfo();

protected: 
	PersonType m_type;

private: 
	char* m_id;
	char* m_firstName;
	char* m_lastName;
	static int nrObjects;
};
