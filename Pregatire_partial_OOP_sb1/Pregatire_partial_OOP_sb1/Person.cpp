#include "Person.h"

int Person::nrObjects = 0;

Person::Person()
{
    this->m_id = new char[100];
    this->m_firstName = new char[100];
    this->m_lastName = new char[100];
    this->m_type = CIVILIAN;
}

Person::Person(char* id, char* firstName, char* lastName)
{
    this->m_id = new char[strlen(id)+1];
    for (unsigned int i = 0; i < strlen(id); i++) {
        this->m_id[i] = id[i];
    }
    this->m_id[strlen(id)] = '\0';

    this->m_firstName = new char[strlen(firstName) + 1];
    for (unsigned int i = 0; i < strlen(firstName); i++) {
        this->m_firstName[i] = firstName[i];
    }
    this->m_firstName[strlen(firstName)] = '\0';

    this->m_lastName = new char[strlen(lastName) + 1];
    for (unsigned int i = 0; i < strlen(lastName); i++) {
        this->m_lastName[i] = lastName[i];
    }
    this->m_lastName[strlen(lastName)] = '\0';
    m_type = CIVILIAN;
    nrObjects++;
}

Person::Person(const Person& other)
{
    this->m_id = new char[strlen(other.m_id) + 1];
    for (unsigned int i = 0; i < strlen(other.m_id); i++) {
        this->m_id[i] = other.m_id[i];
    }
    this->m_id[strlen(other.m_id)] = '\0';

    this->m_firstName = new char[strlen(other.m_firstName) + 1];
    for (unsigned int i = 0; i < strlen(other.m_firstName); i++) {
        this->m_firstName[i] = other.m_firstName[i];
    }
    this->m_firstName[strlen(other.m_firstName)] = '\0';

    this->m_lastName = new char[strlen(other.m_lastName) + 1];
    for (unsigned int i = 0; i < strlen(other.m_lastName); i++) {
        this->m_lastName[i] = other.m_lastName[i];
    }
    this->m_lastName[strlen(other.m_lastName)] = '\0';
    m_type = other.m_type;
}

Person::~Person()
{
    delete[] this->m_id;
    this->m_id = nullptr;

    delete[] this->m_firstName;
    this->m_firstName = nullptr;

    delete[] this->m_lastName;
    this->m_lastName = nullptr;
}

Person& Person::operator=(const Person& other)
{
    if (this == &other) {
        delete[] this->m_id;
        this->m_id = nullptr;

        delete[] this->m_firstName;
        this->m_firstName = nullptr;

        delete[] this->m_lastName;
        this->m_lastName = nullptr;
    }
    this->m_id = new char[strlen(other.m_id) + 1];
    for (unsigned int i = 0; i < strlen(other.m_id); i++) {
        this->m_id[i] = other.m_id[i];
    }
    this->m_id[strlen(other.m_id)] = '\0';

    this->m_firstName = new char[strlen(other.m_firstName) + 1];
    for (unsigned int i = 0; i < strlen(other.m_firstName); i++) {
        this->m_firstName[i] = other.m_firstName[i];
    }
    this->m_firstName[strlen(other.m_firstName)] = '\0';

    this->m_lastName = new char[strlen(other.m_lastName) + 1];
    for (unsigned int i = 0; i < strlen(other.m_lastName); i++) {
        this->m_lastName[i] = other.m_lastName[i];
    }
    this->m_lastName[strlen(other.m_lastName)] = '\0';
    m_type = other.m_type;
    return *this;
}

char* Person::getID() const
{
    return m_id;
}

char* Person::getFirstName() const
{
    return m_firstName;
}

void Person::setFirstName(char* newFirstName)
{
    this->m_firstName = newFirstName;
}

char* Person::getLastName() const
{
    return m_lastName;
}

void Person::setLastName(char* newLastName)
{
    this->m_lastName = newLastName;
}

PersonType Person::getType() const
{
    return m_type;
}

int Person::getNrObjects()
{
    return nrObjects;
}

void Person::displayInfo()
{
    cout << "This is " << m_firstName << " " << m_lastName << " and he/she is a civilian." << endl;
}

ostream& operator<<(ostream& os, const Person& p)
{
    os << "This is " << p.m_firstName << " " << p.m_lastName<< "and he/she is a civilian." <<endl;
    return os;
}

