#include "Person.h"
#include "Student.h"
#include "CoffeeShop.h"
#include<iostream>
using namespace std;

int main() {
	char id1[] = "1";
	char fn1[] = "Joseph";
	char ln1[] = "Joestar";

	char id2[] = "2";
	char fn2[] = "Jotaro";
	char ln2[] = "Kujo";

	char id3[] = "3";
	char fn3[] = "Koichi";
	char ln3[] = "Hirose";

	char id4[] = "4";
	char fn4[] = "Giorno";
	char ln4[] = "Giovanna";

	char id5[] = "5";
	char fn5[] = "Bruno";
	char ln5[] = "Bucellati";

	char id6[] = "6";
	char fn6[] = "Panacotta";
	char ln6[] = "Fugo";

	Person p1{ id1, fn1, ln1 };
	Student s1{ id2, fn2, ln2, 12 };
	Person p2{ id3, fn3, ln3 };
	Student s2{ id4, fn4, ln4, 15 };
	Person p3{ id5, fn5, ln5 };
	Student s3{ id6, fn6, ln6, 16 };

	p1.displayInfo();
	s1.displayInfo();
	p2.displayInfo();
	s2.displayInfo();
	p3.displayInfo();
	s3.displayInfo();
	cout<< "~~~~~~~~~~~~~~";

	CoffeeShop Pazzione{ 100 };
	Pazzione.enqueue(&p1);
	Pazzione.enqueue(&s1);
	Pazzione.enqueue(&p2);
	Pazzione.enqueue(&s2);
	Pazzione.enqueue(&p3);
	Pazzione.enqueue(&s3);
	cout << "Welcome to Pazzione! Meet our guests: " << endl;
	Pazzione.serveClients();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << '\n';
	cout << "Problem 4." << endl;
	cout << "The constructor has been called " << Person::getNrObjects() << " times.";

	return 0;
}
