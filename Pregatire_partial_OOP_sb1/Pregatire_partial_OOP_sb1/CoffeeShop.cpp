#include "CoffeeShop.h"

CoffeeShop::CoffeeShop(int capacity)
{
	this->m_cap = capacity;
	this->m_nrPeople = 0;
	this->m_people = new Person*[capacity];
}

ostream& operator<<(ostream& os, const CoffeeShop& other)
{
	for (int i = 0; i < other.m_nrPeople; i++) {
		os << other.m_people[i];
	}
	return os;
}

void CoffeeShop::enqueue(Person* p)
{
	if (this->m_nrPeople == this->m_cap) {
		cout << "Sorry, the caffee is full! PLease come back some other time!";
	}
	else {
		this->m_people[this->m_nrPeople++] = p;
	}
}

void CoffeeShop::serveClients()
{
	Person** people = new Person*[100];
	for (int i = 0; i < this->m_nrPeople; i++) {
		if (this->m_people[i]->getType() == STUDENT) {
			this->m_people[i]->displayInfo();
		}
	}
	for (int i = 0; i < this->m_nrPeople; i++) {
		if (this->m_people[i]->getType() == CIVILIAN) {
			this->m_people[i]->displayInfo();
		}
	}
}
