#include "Student.h"

Student::Student()
{
    this->m_enrollmentNumber = 0;
    this->m_type = STUDENT;
}

Student::Student(char* id, char* firstName, char* lastName, int enrollmentNumber) : Person(id, firstName, lastName)
{
    m_enrollmentNumber = enrollmentNumber;
    m_type = STUDENT;
}

void Student::displayInfo()
{
    Person::displayInfo();
    cout << "This is " << Person::getFirstName() << " " << Person::getLastName() << " and he/she is a student." << endl;
}


int Student::getEnrollmentNumber() const
{
    return m_enrollmentNumber;
}

ostream& operator<<(ostream& os, const Student& other)
{
    os << "This is student " << other.getFirstName() << " " << other.getLastName();
    return os;
}
