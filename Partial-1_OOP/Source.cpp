#include <stdlib.h>
#include <iostream>
using namespace std;

#include"Point.h"
#include"Tortoise.h"
#include"Rabbit.h"
#include"Character.h"

int main() {

	Character** array = new Character*[3];


	Tortoise Tortoise1(1, Point(0, 0), "Tortoise");
	Tortoise Tortoise2(2, Point(0, 0), "Tortoise");
	Rabbit Rabbit1(3, Point(0, 0), "Rabbit");

	array[0] = &Tortoise1;
	array[1] = &Tortoise2;
	array[2] = &Rabbit1;

	int numCharacters = 3;

	for (int ci = 0; ci < numCharacters; ci++) {
		int x = rand() % 101;
		int y = rand() % 101;

		array[ci]->setPos(Point(x, y));

	}

	const int simulationSteps = 100;
	for (int i = 0; i < simulationSteps; i++) {
		int dir = rand() % 4;
		array[0]->move(dir);
	}
	
	for (int i = 0; i < simulationSteps; i++) {
		int dir = rand() % 4;
		array[1]->move(dir);
	}

	for (int i = 0; i < simulationSteps; i++) {
		int dir = rand() % 4;
		array[2]->move(dir);
	}

	for (int i = 0; i < numCharacters; i++) {
		cout << array[i]->getId() << " " << array[i]->getType() << " ";
		array[i]->displayBag();
		cout << endl;
	}

	return 0;
}