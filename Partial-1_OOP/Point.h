#pragma once
#include<iostream>
using namespace std;

class Point {
public:
	Point();
	Point(unsigned int x, unsigned int y);

	int const getX();
	void setX(unsigned int x);
	int const getY();
	void setY(unsigned int y);

	friend ostream& operator <<(ostream& out, const Point& c);
	friend istream& operator >>(istream& in, Point& c);

	Point& operator=(const Point& other);
	Point& operator+(const Point& other);

private:
	int m_x, m_y;
};