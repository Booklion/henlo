#pragma once
#include"Character.h"
#include"Point.h"
#include<iostream>
#include<math.h>
#include<cstring>
using namespace std;

class Tortoise : public Character {
public:
	Tortoise();
	Tortoise(int id, Point position, string Type);

	void move(int direction);
	void addChocolate(Point at_position);
};