#pragma once
#include"Character.h"
#include"Point.h"
#include<iostream>
#include<math.h>
#include<cstring>
using namespace std;

class Rabbit : public Character {
public:
	Rabbit();
	Rabbit(int id, Point position, string Type);
	void move(int direction);
	void addChocolate(Point at_position);
};