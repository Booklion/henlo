#include "Point.h"

Point::Point()
{
	m_x = 0;
	m_y = 0;
}

Point::Point(unsigned int x, unsigned int y)
{
	m_x = x;
	m_y = y;
}

int const Point::getX()
{
	return m_x;
}

void Point::setX(unsigned int x)
{
	m_x = x;
}

int const Point::getY()
{
	return m_y;
}

void Point::setY(unsigned int y)
{
	m_y = y;
}

Point& Point::operator=(const Point& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
	return (*this);
}

Point& Point::operator+(const Point& other)
{
	m_x = m_x + other.m_x;
	m_y = m_y + other.m_y;
	return (*this);
}

ostream& operator<<(ostream& out, const Point& c)
{
	out << c.m_x << " " << c.m_y;
	return out;
}

istream& operator>>(istream& in, Point& c)
{
	in >> c.m_x >> c.m_y;
	return in;
}