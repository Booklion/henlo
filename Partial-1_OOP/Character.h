#pragma once
#include"Point.h"
#include<iostream>
#include<cstring>
using namespace std;

class Character {
public:
	Character();
	Character(int id, Point position, string Type);
	Character(const Character& other);
	~Character();

	void freeMemory();

	virtual void move(int direction);

	void setPos(Point position);
	void setId(int id);
	void setCounter(int counter);
	void addToBag(int weight);
	void setType(string Type);

	Point const getPos();
	int const getId();
	void const displayBag();
	string getType();

private:
	int m_id;
	Point m_position;
	int* m_bag, m_chocolate_counter;
	string m_type;
};