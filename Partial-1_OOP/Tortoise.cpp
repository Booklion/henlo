#include "Tortoise.h"
#include"Point.h"
#include"Character.h"

Tortoise::Tortoise() : Character()
{
	setId(0);
	setCounter(0);
}

Tortoise::Tortoise(int id, Point position, string Type) : Character(id, position, "Tortoise")
{
}

void Tortoise::move(int direction)
{
	Point newPoint = getPos();
	if (direction == 0) {
		newPoint = newPoint + Point(-3, 0);
	}
	else if (direction == 1) {
		newPoint = newPoint + Point(3, 0);
	}
	else if (direction == 2) {
		newPoint = newPoint + Point(0, 3);
	}
	else if (direction == 3) {
		newPoint = newPoint + Point(0, -3);
	}
	else {
		cout << "Illegal move!" << endl;
	}
	setPos(newPoint);
	addChocolate(newPoint);
}

void Tortoise::addChocolate(Point at_position)
{
	int weight = 0;
	int x = at_position.getX();
	int y = at_position.getY();
	if (x % 2 == 0 && y % 2 != 0) {
		weight = abs(x + y);
	}
	addToBag(weight);
}
