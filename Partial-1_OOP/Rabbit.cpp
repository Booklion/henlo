#include"Character.h"
#include "Rabbit.h"
#include"Point.h"

Rabbit::Rabbit() : Character()
{
	setId(0);
	setCounter(0);
}

Rabbit::Rabbit(int id, Point position, string type) : Character(id, position, "Rabbit")
{
}

void Rabbit::move(int direction)
{
	Point newPoint = getPos();
	if (direction == 0) {
		newPoint = newPoint + Point(-4, 0);
	}
	else if (direction == 1) {
		newPoint = newPoint + Point(4, 0);
	}
	else if (direction == 2) {
		newPoint = newPoint + Point(0, 4);
	}
	else if (direction == 3) {
		newPoint = newPoint + Point(0, -4);
	}
	else {
		cout << "Illegal move!" << endl;
	}
	setPos(newPoint);
	addChocolate(newPoint);
}

void Rabbit::addChocolate(Point at_position)
{
	int weight = 0;
	int x = at_position.getX();
	int y = at_position.getY();
	if (x % 3 == 0 || y % 3 == 0) {
		weight = abs(x);
	}
	addToBag(weight);
}
