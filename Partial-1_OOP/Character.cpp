#include "Character.h"
#include<cstring>

Character::Character()
{
	m_position = Point(0, 0);		// Technically start at (0,0)
	m_id = 0;						// Also let the id be 0 meaning it doesn't exist
	if (m_bag != NULL) {
		delete[] m_bag;
		m_bag = nullptr;
	}
	m_bag = new int[100];
	for (int i = 0; i < 100; i++) {	// If the weight of the chocolate bar is 0 that means it doesn't exist
		m_bag[i] = 0;				// Logically speaking
	}
	m_chocolate_counter = 0;
}

Character::Character(int id, Point position, string type)
{
	m_type = type;
	m_id = id;
	m_position = position;
	if (m_bag != NULL) {
		delete[] m_bag;
		m_bag = nullptr;
	}
	m_bag = new int[100];
	for (int i = 0; i < 100; i++) {	// If the weight of the chocolate bar is 0 that means it doesn't exist
		m_bag[i] = 0;				// Logically speaking
	}
	m_chocolate_counter = 0;
}

Character::Character(const Character& other)
{
	m_id = other.m_id;
	m_position = other.m_position;
	if (m_bag != NULL) {
		delete[] m_bag;
		m_bag = nullptr;
	}
	m_bag = new int[100];
	for (int i = 0; i < 100; i++) {
		m_bag[i] = other.m_bag[i];
	}
	m_chocolate_counter = other.m_chocolate_counter;
}

Character::~Character()
{
	freeMemory();
}

void Character::freeMemory()
{
	m_id = 0;
	m_position = Point(0, 0);
	m_chocolate_counter = 0;
	if (m_bag != NULL) {
		delete[] m_bag;
		m_bag = nullptr;
	}
}

void Character::move(int direction)
{
	if (direction == 0) {
		m_position = m_position + Point(-1, 0);
	}
	else if (direction == 1) {

		m_position = m_position + Point(1, 0);
	}
	else if (direction == 2) {

		m_position = m_position + Point(0, 1);
	}
	else if (direction == 3) {

		m_position = m_position + Point(0, -1);
	}
	else {
		cout << "Illegal move!" << endl;
	}
}

void Character::setPos(Point position)
{
	m_position = position;
}

void Character::setId(int id)
{
	m_id = id;
}

void Character::setCounter(int counter)
{
	m_chocolate_counter = counter;
}

void Character::addToBag(int weight)
{
	if (weight > 0) {	// Since a weight smaller than 0 would create a black hole
		m_chocolate_counter++;
		m_bag[m_chocolate_counter] = weight;
	}
}

void Character::setType(string Type)
{
	m_type = Type;
}

Point const Character::getPos()
{
	return m_position;
}

int const Character::getId()
{
	return m_id;
}

void const Character::displayBag()
{
	for (int i = 0; i < 100; i++) {
		if (m_bag[i] > 0) {		// If the weight is negative then we create a black hole
			cout << m_bag[i] << " ";	
		}
	}
}

string Character::getType()
{
	return m_type;
}
