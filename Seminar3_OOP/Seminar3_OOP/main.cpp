#include "AcousticWave.h"

int main() {
	AcousticWave a;
	a.computeSamples(1, 20000, 44100);

	SineWave s;
	s.computeSamples(1, 20000, 44100);
	WaveOutput::saveSamples("sin.csv", &s); //metodele statice se pot apela si asa
	WaveOutput b;
	b.saveSamples("sin.csv", &s);

	SquareWave w;
	w.computeSamples(1, 20000, 44100);
	WaveOutput c;
	c.saveSamples("square.csv", &w);

	TriangleWave t;
	t.computeSamples(1, 20000, 44100);
	WaveOutput::saveSamples("triangle.csv", &t);

	SawToothWave u;
	u.computeSamples(1, 4, 2000);
	WaveOutput::saveSamples("sawtooth.csv", &u);

	return 0;
}
