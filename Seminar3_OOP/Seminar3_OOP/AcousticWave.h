#pragma once
#include <ostream>
class AcousticWave {
public:
	//default constructor
	AcousticWave();

	//copy constructor
	AcousticWave(const AcousticWave& other);
	
	//destructor
	~AcousticWave();

	//assignment operator
	AcousticWave& operator=(const AcousticWave& other);

	//virtual method
	void computeSamples(float duration, float frequency, float sampleRate);

	//Overloading the operator * for performing amplitude modulation on two sound samples
	//The amplitude of the carrier signal is varied in accordance with the instantaneous amplitude of the modulating signal
	AcousticWave operator*(const AcousticWave& w);

private:				//if marked as protected, the attributes become inaccessible to the other classes when trying to overload the << operator
	float* m_sampleArray;
	int m_arrayLength;
	float m_frequency;
	float m_amplitude;
	int m_sampleRate;
	virtual float getSampleValue(float frequency, float amplitude, int t);
	friend class WaveOutput;
};

class SineWave : public AcousticWave {
public:
	float getSampleValue(float frequency, float amplitude, int t);
	//friend ostream &operator<<(ostream& os, const SineWave& w);
};

class SquareWave : public AcousticWave {
	float getSampleValue(float frequency, float amplitude, int t);
	//friend ostream& operator<<(ostream& os, const SquareWave& w);
};

class TriangleWave : public AcousticWave {
	float getSampleValue(float frequency, float amplitude, int t);
	//friend ostream& operator<<(ostream& os, const TriangleWave& w);
};

class SawToothWave : public AcousticWave {
	float getSampleValue(float frequency, float amplitude, int t);
	//friend ostream& operator<<(ostream& os, const SawToothWave& w);
};

class WaveOutput {
public:
	static bool saveSamples(const char* csv_path, AcousticWave* w);
};
