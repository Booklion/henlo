#include "AcousticWave.h"
#include<math.h>
#include<fstream>
using namespace std;


//From class AcousticWave:
AcousticWave::AcousticWave() : m_sampleArray{ nullptr }, m_frequency{ 0 }, m_sampleRate{ 0 }, m_arrayLength{0} 
{
}

AcousticWave::AcousticWave(const AcousticWave& other) {
	m_sampleArray = new float[other.m_arrayLength];
	this->m_arrayLength = other.m_arrayLength;
	m_frequency = other.m_frequency;
	m_amplitude = other.m_amplitude;
	m_sampleRate = other.m_sampleRate;
	for (int i = 0; i < m_arrayLength; i++) {
		memcpy(other.m_sampleArray, m_sampleArray, sizeof(float) * m_arrayLength);
		m_sampleArray[i] = other.m_sampleArray[i];
	}
}

AcousticWave::~AcousticWave() {
	delete &m_frequency;
	delete& m_amplitude;
	delete& m_arrayLength;
	delete& m_sampleRate;
	delete[] m_sampleArray;
	m_sampleArray = nullptr;
}

AcousticWave& AcousticWave::operator=(const AcousticWave& other) {
	m_frequency = other.m_frequency;
	m_amplitude = other.m_amplitude;
	m_arrayLength = other.m_arrayLength;
	m_sampleRate = other.m_sampleRate;
	for (int i = 0; i < m_arrayLength; i++) {
		m_sampleArray[i] = other.m_sampleArray[i];
	}
	return *this;
}

void AcousticWave::computeSamples(float duration, float frequency, float sampleRate) {
	//duration in seconds
	if (m_sampleArray != nullptr) {
		delete[] m_sampleArray;
		m_sampleArray = nullptr;
	}
	float amplitude = 1;
	const float pi = 3.14;
	m_sampleArray = new float[duration * sampleRate](); //the square brackets () initialise with 0
	m_frequency = frequency;
	m_sampleRate = sampleRate;
	m_arrayLength = duration * sampleRate;
	for (int t = 0; t < m_arrayLength; t++) {
		m_sampleArray[t] = getSampleValue(frequency, amplitude, t);
	}
}

float AcousticWave::getSampleValue(float frequency, float amplitude, int t) {
	return 0.0f;
}

AcousticWave AcousticWave::operator*(const AcousticWave& w) {
	AcousticWave result;
	for (int i = 0; i < m_arrayLength; i++) {
		result.m_sampleArray[i] = m_sampleArray[i] * w.m_sampleArray[i];
		delete &m_sampleArray[i];
		m_sampleArray[i] = result.m_sampleArray[i];
	}
	return result;
}


//From class WaveOutput:
bool WaveOutput::saveSamples(const char* csv_patch, AcousticWave* w) {
	ofstream outputFile;
	outputFile.open(csv_patch);
	for (int i = 0; i < w->m_arrayLength; i++) {
		outputFile << w->m_sampleArray[i] << endl;
	}
	outputFile.close();
	return 1;
}


//From class SineWave:
float SineWave::getSampleValue(float frequency, float amplitude, int t) {
	const float pi = 3.14;
	return amplitude * sin(2 * pi * frequency * t);
}
/*
ostream& operator<<(ostream& os, const SineWave& w)
{
	os << "Sine Wave: " << w.m_frequency << ", " << w.m_sampleArray;
	return os;
}
*/

//From class SquareWave:
float SquareWave::getSampleValue(float frequency, float amplitude, int t) {
	const float pi = 3.14;
	if (sin(2 * pi * frequency * t) > 0) {
		return 1;
	}
	else {
		return -1;
	}
}
/*
ostream& operator<<(ostream& os, const SquareWave& w) {
	os << "Square Wave: " << w.m_frequency << ", " << w.m_sampleArray;
	return os;
}
*/

//From class TriangleWave:
float TriangleWave::getSampleValue(float frequency, float amplitude, int t) {
	const float pi = 3.14;
	return (2 * amplitude * asin(sin(2 * pi * frequency * t))) / pi;
}
/*
ostream& operator<<(ostream& os, const TriangleWave& w) {
	os << "Triangle Wave: " << w.m_frequency << ", " << w.m_sampleArray;
	return os;
}
*/

//From class SawToothWave:
float SawToothWave::getSampleValue(float frequency, float amplitude, int t) {
	const float pi = 3.14;
	return (-2 * amplitude * atan(cos(pi * frequency * t))) / pi;
}
/*
ostream& operator<<(ostream& os, const SawToothWave& w) {
	os << "Saw Tooth Wave: " << w.m_frequency << ", " << w.m_sampleArray;
	return os;
}
*/